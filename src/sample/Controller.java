package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import sun.nio.ch.Interruptible;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public DatePicker birthday;
    public ToggleGroup sexGroup;
    public RadioButton rbB;
    public RadioButton rbFree;
    public RadioButton rbG;
    public GridPane myCtrl;
    public javafx.scene.image.ImageView myAatar;
    public Button btnSelectAvatar;
    public ComboBox<String> cbBlood;
    public ComboBox<String> cbplace;

    public void doSelectBirthday(ActionEvent actionEvent) {
        System.out.println(birthday.getValue());
    }


    public void doSelectSex(javafx.event.ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton) actionEvent.getSource();
        System.out.println(radioButton.getText());
    }

    public void doSelectAvertar(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("找圖片");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("影像檔", "*.png", "*.jpg", "*.gif"));
        File selectedFile = fileChooser.showOpenDialog(myCtrl.getScene().getWindow());
        System.out.println(selectedFile.getAbsolutePath());

        myAatar.setImage(new Image(selectedFile.toURI().toString()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resource) {
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("A", "O", "P");
        cbBlood.getItems().addAll(items);

    ObservableList<String> itemplaces = FXCollections.observableArrayList();

    String filename = "C:\\place.txt";
    String line;

        try {
            BufferedReader  bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while((line=bufferedReader.readLine())!=null){
                itemplaces.add(line);
            }
            bufferedReader.close();
            cbplace.setItems(itemplaces);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

